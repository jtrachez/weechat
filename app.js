var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var _ = require('underscore');
var mongoose = require('mongoose')

/*
Variables
*/
var username;
var usernames = {};
var nb_users = 0;


/*
DB & Models
*/
mongoose.connect('mongodb://localhost/quizzy');
var db = mongoose.connection;
var UsersDb = mongoose.model('Users', { username: String });

// UsersDb.find(function(err,data){
// 	if(err) console.log(err)
// 	if(data) console.log(data)
// })


/*
Server
*/
var Server = function(){};

Server.prototype.__addUser = function(socket) {

	socket.on('add.user', function(user){
		username = user.input;
		socket.username = username;
		usernames[username] = username;
		nb_users++
		Server.__sendNbUsers(socket);

		console.log('[SERVER] New user ' + username)
		// UsersDb.create({username:username},function(err,data){
		// 	if(err) console.log(err)
		// 	else console.log(data)
		// });
		socket.emit('added.user',{user:username,nb_users:nb_users})
		io.sockets.emit('joined.user',{user:username,nb_users:nb_users})
		
	})

};

Server.prototype.__sendUserMessage = function(socket){
	socket.on('user.msg', function(data){
		console.log('[CLIENT] New message from '+data.username)
		socket.broadcast.emit('user.msg.added',{msg:data.input,username:data.username});
	})
}
Server.prototype.__disconnectUser = function(socket) {
	socket.on('disconnect',function(){
		console.log('USER disconnect')
			if(username)
				console.log(usernames[socket.username])
				delete usernames[socket.username];
				--nb_users
				Server.__sendNbUsers(socket);
		

	})
};

Server.prototype.__sendNbUsers = function(socket) {
	if( nb_users < 0){nb_users = 0}
	var send = {
		user:username,
		nb_users:nb_users,
		usernames:usernames
	}
	io.sockets.emit('nb_user.change',send)
};

Server = new Server(io);


/*
Start HTTP server
*/
http.listen(8080,function(){
	console.log('[SERVER] : Start Quizzy...')
})

/*
Middleware
*/
app.use(express.static(__dirname + '/public'));


/*
Events
*/
io.sockets.on('connection', function(socket){
	console.log('[CLIENT] Connexion...')
	Server.__sendNbUsers(socket)
	Server.__addUser(socket)
	Server.__sendUserMessage(socket)
	Server.__disconnectUser(socket)

})

